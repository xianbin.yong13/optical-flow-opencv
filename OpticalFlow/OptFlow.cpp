#include "OptFlow.hpp"

#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>

# if __has_include(<filesystem>)
#   include <filesystem>
# else
#   include <experimental/filesystem>
namespace std {
    namespace filesystem = experimental::filesystem;
}
# endif

#include <opencv2/core.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/cudaoptflow.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudaarithm.hpp>


using namespace std;
using namespace cv;
using namespace cv::cuda;

string OptFlow::padZero(int num)
{
    ostringstream ss;
    ss << setw(3) << setfill('0') << num;
    return ss.str();
}

string OptFlow::to_precision(double value, int precision = 2) {
    ostringstream ss;
    ss << std::fixed << std::setprecision(precision) << value;
    return ss.str();
}

//https://stackoverflow.com/questions/32332920/efficiently-load-a-large-mat-into-memory-in-opencv/32357875
void OptFlow::matwrite(const string& filename, const Mat& mat)
{
    std::ofstream fs(filename, std::fstream::binary);

    // Header
    int type = mat.type();
    //fs.write((char*)&mat.rows, sizeof(int));    // rows
    //fs.write((char*)&mat.cols, sizeof(int));    // cols
    //fs.write((char*)&type, sizeof(int));        // type
                                                // Data
    if (mat.isContinuous())
    {
        fs.write(mat.ptr<char>(0), (mat.dataend - mat.datastart));
    }
    else
    {
        int rowsz = CV_ELEM_SIZE(type) * mat.cols;
        for (int r = 0; r < mat.rows; ++r)
        {
            fs.write(mat.ptr<char>(r), rowsz);
        }
    }

    fs.close();
}

string OptFlow::dirnameOf(const string& fname)
{
    size_t pos = fname.find_last_of("\\/");
    return(fname.substr(0, pos));
}

string OptFlow::remove_extension(string filename) {
    size_t lastdot = filename.find_last_of(".");
    if (lastdot == std::string::npos) {
        return filename;
    }
    return filename.substr(0, lastdot);
}


void OptFlow::writeFlow(std::filesystem::path parent_dir, int i, const GpuMat& d_flow, bool use_half_precision, bool save_as_img)
{
    GpuMat planes[2];
    string dtype;
    cuda::split(d_flow, planes);

    Mat flowx(planes[0]);
    Mat flowy(planes[1]);

    std::filesystem::path flowx_dir = parent_dir;
    flowx_dir /= "flowx";
    std::filesystem::path flowy_dir = parent_dir;
    flowy_dir /= "flowy";

    std::filesystem::create_directory(flowx_dir);
    std::filesystem::create_directory(flowy_dir);

    if (save_as_img) {
        if (use_half_precision == true) {
            cerr << "Warning: Half prevision is not supported while saving images as TIFF" << endl;
        }
        std::filesystem::path flowx_path = flowx_dir / ("flowx_" + padZero(i) + dtype + ".tif");
        std::filesystem::path flowy_path = flowy_dir / ("flowy_" + padZero(i) + dtype + ".tif");

        imwrite(flowx_path.string(), flowx);
        cout << "FlowX saved to: " << flowx_path << endl;

        imwrite(flowy_path.string(), flowy);
        cout << "FlowY saved to: " << flowy_path << endl;
    }
    else {
        if (use_half_precision == true) {
            flowx.convertTo(flowx, CV_16F);
            flowy.convertTo(flowy, CV_16F);
            dtype = "_float16";
        }
        else {
            dtype = "_float32";
        }

        std::filesystem::path flowx_path = flowx_dir / ("flowx_" + padZero(i) + dtype + ".bin");
        std::filesystem::path flowy_path = flowy_dir / ("flowy_" + padZero(i) + dtype + ".bin");

        matwrite(flowx_path.string(), flowx);
        cout << "FlowX saved to: " << flowx_path << endl;

        matwrite(flowy_path.string(), flowy);
        cout << "FlowY saved to: " << flowy_path << endl;
    }
}

void OptFlow::writeCost(std::filesystem::path parent_dir, int i, const GpuMat& d_flow_cost)
{
    Mat flow_cost(d_flow_cost);

    std::filesystem::path flow_cost_path = parent_dir;
    flow_cost_path /= "flow_cost";

    std::filesystem::create_directory(flow_cost_path);

    flow_cost_path /= ("flow_cost_" + padZero(i) + ".bin");

    matwrite(flow_cost_path.string(), flow_cost);
    cout << "FlowCost saved to: " << flow_cost_path << endl;
}

Mat OptFlow::imadjust(const Mat src, Vec2d in = Vec2d(0, 1)) {
    // in : src image bounds
    // out : dst image buonds 
    Mat dst = src.clone();
    int img_depth = src.depth();
    Vec2i in_value;
    Vec2i out_value;

    int max_value = 255;
    if (img_depth == CV_16U) {
        max_value = 65535;
    }

    int histSize[] = { max_value };

    float hranges[] = { 0.0, static_cast<float>(max_value) };
    const float* ranges[] = { hranges };
    int channels[] = { 0 };

    Mat hist;
    calcHist(&src, 1, channels, Mat(), hist, 1, histSize, ranges, true, false);

    int total = src.rows * src.cols;
    int low_bound = total * in[0];
    int upp_bound = total * in[1];

    // Cumulative histogram 
    Mat accumulatedHist = hist.clone();
    for (int i = 1; i < max_value; i++) {
        accumulatedHist.at<float>(i) += accumulatedHist.at<float>(i - 1);
        if (in_value[0] == 0 && accumulatedHist.at<float>(i) > low_bound) {
            in_value[0] = i;
        }
        if (in_value[1] == 0 && accumulatedHist.at<float>(i) > upp_bound) {
            in_value[1] = i;
        }
    }

    out_value[0] = 0;
    out_value[1] = max_value;

    // Stretching 
    dst = (dst - in_value[0]) * (out_value[1] - out_value[0]) / (in_value[1] - in_value[0]);
    dst = max(dst, 0);
    dst = min(dst, max_value);

    return dst;
}

Mat OptFlow::loadImage(Mat multi_frame) {
    Mat frame;

    if (multi_frame.depth() == CV_16U) {
        multi_frame.convertTo(frame, CV_8U, 1 / 256.0);
    }
    else {
        frame = multi_frame;
    }

    return frame;
}

tuple<GpuMat, GpuMat, GpuMat> OptFlow::loadImagePair(int pos, vector<Mat> multi_frame, bool is_reverse) {
    GpuMat d_frame0, d_frame1;
    int next_pos;

    if (is_reverse) {
        int length = static_cast<int>(multi_frame.size());
        next_pos = length - pos - 2;
        pos = length - pos - 1;
    }
    else {
        next_pos = pos + 1;
    }

    Mat frame0 = loadImage(multi_frame[pos]);
    d_frame0.upload(frame0);

    Mat frame1 = loadImage(multi_frame[next_pos]);
    d_frame1.upload(frame1);

    GpuMat d_flow(frame0.size(), CV_32FC2);

    return { d_frame0, d_frame1, d_flow };
}


void PyrLK::runOptFlow() {
    int frame_no = multi_frame.size();
    Ptr<cuda::DensePyrLKOpticalFlow> d_optflow = cuda::DensePyrLKOpticalFlow::create(Size(winSize, winSize), numLevels, numIters);
    
    for (int i = 0; i < frame_no - 1; i++)
    {
        //ouput image details
        auto [d_frame0, d_frame1, d_flow] = loadImagePair(i, multi_frame, is_reverse);
        cout << "Image " << to_string(i+1) << " size : " << d_frame0.cols << " x " << d_frame0.rows << endl;
        d_optflow->calc(d_frame0, d_frame1, d_flow);
        
        writeFlow(parent_dir, i+1, d_flow, use_half_precision, save_as_img);
    }
}


PyrLK::PyrLK(int winSize, int numLevels, int numIters, bool is_reverse, bool use_half_precision, bool save_as_img, std::filesystem::path fpath) : \
winSize(winSize), numLevels(numLevels), numIters(numIters), is_reverse(is_reverse), use_half_precision(use_half_precision), save_as_img(save_as_img) \
{
    std::filesystem::path pathObj(fpath);
    cout << "Loading file: " << fpath.string() << endl;
    imreadmulti(fpath.string(), multi_frame, IMREAD_UNCHANGED);
    parent_dir = dirnameOf(fpath.string());

    if (is_reverse) {
        parent_dir /= ("PyrLK_" + to_string(numLevels) + "_" + to_string(winSize) + "_" + to_string(numIters) + "_reverse");
    }
    else {
        parent_dir /= ("PyrLK_" + to_string(numLevels) + "_" + to_string(winSize) + "_" + to_string(numIters));
    }
    std::filesystem::create_directory(parent_dir.c_str());
    cout << "Output Directory: " << parent_dir << endl;
};


void Farneback::runOptFlow() {
    int frame_no = multi_frame.size();
    Ptr<cuda::FarnebackOpticalFlow> d_optflow = cuda::FarnebackOpticalFlow::create(numLevels, pyrScale, fastPyramids, winSize, numIters, polyN, polySigma);

    for (int i = 0; i < frame_no - 1; i++)
    {
        //ouput image details
        auto [d_frame0, d_frame1, d_flow] = loadImagePair(i, multi_frame, is_reverse);
        cout << "Image " << to_string(i + 1) << " size : " << d_frame0.cols << " x " << d_frame0.rows << endl;
        d_optflow->calc(d_frame0, d_frame1, d_flow);

        writeFlow(parent_dir, i + 1, d_flow, use_half_precision, save_as_img);
    }
}


Farneback::Farneback(int numLevels, double pyrScale, bool fastPyramids, int winSize, int numIters, int polyN, double polySigma, bool is_reverse, bool use_half_precision, bool save_as_img, std::filesystem::path fpath) : \
numLevels(numLevels), pyrScale(pyrScale), fastPyramids(fastPyramids), winSize(winSize), numIters(numIters), polyN(polyN), polySigma(polySigma), is_reverse(is_reverse), use_half_precision(use_half_precision), save_as_img(save_as_img) \
{
    std::filesystem::path pathObj(fpath);
    cout << "Loading file: " << fpath.string() << endl;
    imreadmulti(fpath.string(), multi_frame, IMREAD_UNCHANGED);
    parent_dir = dirnameOf(fpath.string());

    if (is_reverse) {
        parent_dir /= ("Farneback_" + to_string(numLevels) + "_" + to_string(winSize) + "_" + to_string(numIters) + "_reverse");
    }
    else {
        parent_dir /= ("Farneback_" + to_string(numLevels) + "_" + to_string(winSize) + "_" + to_string(numIters));
    }
    std::filesystem::create_directory(parent_dir.c_str());
    cout << "Output Directory: " << parent_dir << endl;
};


void Brox::runOptFlow() {
    int frame_no = multi_frame.size();
    Ptr<cuda::BroxOpticalFlow> d_optflow = cuda::BroxOpticalFlow::create(alpha, gamma, pyrScale, innerIter, outerIter, solverIter);

    for (int i = 0; i < frame_no - 1; i++)
    {
        //ouput image details
        auto [d_frame0, d_frame1, d_flow] = loadImagePair(i, multi_frame, is_reverse);
        cout << "Image " << to_string(i + 1) << " size : " << d_frame0.cols << " x " << d_frame0.rows << endl;
        d_optflow->calc(d_frame0, d_frame1, d_flow);

        writeFlow(parent_dir, i + 1, d_flow, use_half_precision, save_as_img);
    }
}


Brox::Brox(double alpha, double gamma, double pyrScale, int innerIter, int outerIter, int solverIter, bool is_reverse, bool use_half_precision, bool save_as_img, std::filesystem::path fpath) : \
alpha(alpha), gamma(gamma), pyrScale(pyrScale), innerIter(innerIter), outerIter(outerIter), solverIter(solverIter), is_reverse(is_reverse), use_half_precision(use_half_precision), save_as_img(save_as_img) \
{
    std::filesystem::path pathObj(fpath);
    cout << "Loading file: " << fpath.string() << endl;
    imreadmulti(fpath.string(), multi_frame, IMREAD_UNCHANGED);
    parent_dir = dirnameOf(fpath.string());

    if (is_reverse) {
        parent_dir /= ("Brox_" + to_precision(alpha) + "_" + to_precision(gamma) + "_" + to_string(innerIter) + "_" + to_string(outerIter) + "_reverse");
    }
    else {
        parent_dir /= ("Brox_" + to_precision(alpha) + "_" + to_precision(gamma) + "_" + to_string(innerIter) + "_" + to_string(outerIter));
    }

    std::filesystem::create_directory(parent_dir.c_str());
    cout << "Output Directory: " << parent_dir << endl;
};


void DualTVL1::runOptFlow() {
    int frame_no = multi_frame.size();
    Ptr<cuda::OpticalFlowDual_TVL1> d_optflow = cuda::OpticalFlowDual_TVL1::create(tau, lambda, theta, numLevels, numWarps, epsilon, numIters, pyrScale, gamma);

    for (int i = 0; i < frame_no - 1; i++)
    {
        //ouput image details
        auto [d_frame0, d_frame1, d_flow] = loadImagePair(i, multi_frame, is_reverse);
        cout << "Image " << to_string(i + 1) << " size : " << d_frame0.cols << " x " << d_frame0.rows << endl;
        d_optflow->calc(d_frame0, d_frame1, d_flow);

        writeFlow(parent_dir, i + 1, d_flow, use_half_precision, save_as_img);
    }
}

DualTVL1::DualTVL1(double tau, double lambda, double theta, int numLevels, int numWarps, double epsilon, int numIters, double pyrScale, double gamma, bool is_reverse, bool use_half_precision, bool save_as_img, std::filesystem::path fpath) : \
tau(tau), lambda(lambda), theta(theta), numLevels(numLevels), numWarps(numWarps), epsilon(epsilon), numIters(numIters), pyrScale(pyrScale), gamma(gamma), is_reverse(is_reverse), use_half_precision(use_half_precision), save_as_img(save_as_img) \
{
    std::filesystem::path pathObj(fpath);
    cout << "Loading file: " << fpath.string() << endl;
    imreadmulti(fpath.string(), multi_frame, IMREAD_UNCHANGED);
    parent_dir = dirnameOf(fpath.string());

    if (is_reverse) {
        parent_dir /= ("DualTVL1_" + to_string(numLevels) + "_" + to_precision(theta) + "_" + to_precision(lambda) + "_" + to_precision(epsilon) + "_" + to_string(numIters) + "_reverse");
    }
    else {
        parent_dir /= ("DualTVL1_" + to_string(numLevels) + "_" + to_precision(theta) + "_" + to_precision(lambda) + "_" + to_precision(epsilon) + "_" + to_string(numIters));
    }

    std::filesystem::create_directory(parent_dir.c_str());
    cout << "Output Directory: " << parent_dir << endl;
};

void NvidiaOF::runOptFlow() {
    int frame_no = multi_frame.size();
    std::unordered_map<std::string, NvidiaOpticalFlow_1_0::NVIDIA_OF_PERF_LEVEL> presetMap = {
    { "Slow", NvidiaOpticalFlow_1_0::NVIDIA_OF_PERF_LEVEL::NV_OF_PERF_LEVEL_SLOW },
    { "Medium", NvidiaOpticalFlow_1_0::NVIDIA_OF_PERF_LEVEL::NV_OF_PERF_LEVEL_MEDIUM },
    { "Fast", NvidiaOpticalFlow_1_0::NVIDIA_OF_PERF_LEVEL::NV_OF_PERF_LEVEL_FAST } };

    auto search = presetMap.find(perf);
    if (search == presetMap.end())
    {
        std::cout << "Invalid preset level : " << perf << std::endl;
        return;
    }
    NvidiaOpticalFlow_1_0::NVIDIA_OF_PERF_LEVEL perf_preset = search->second;

    // NvidiaOF
    Ptr<cuda::NvidiaOpticalFlow_1_0> d_optflow = cuda::NvidiaOpticalFlow_1_0::create(
        multi_frame[0].size().width, multi_frame[0].size().height, perf_preset, temporal_hints, external_hints, cost_buffer, gpu_id);
    GpuMat d_cost, d_flow_calc;

    for (int i = 0; i < frame_no - 1; i++)
    {
        //ouput image details
        auto [d_frame0, d_frame1, d_flow] = loadImagePair(i, multi_frame, is_reverse);
        cout << "Image " << to_string(i + 1) << " size : " << d_frame0.cols << " x " << d_frame0.rows << endl;
        d_optflow->calc(d_frame0, d_frame1, d_flow_calc, Stream::Null(), cv::noArray(), d_cost);
        d_optflow->upSampler(d_flow_calc, d_frame0.size().width, d_frame0.size().height, d_optflow->getGridSize(), d_flow);

        writeFlow(parent_dir, i + 1, d_flow, use_half_precision, save_as_img);
    }
    d_optflow->collectGarbage();
}

NvidiaOF::NvidiaOF(string perf, bool cost_buffer, bool external_hints, bool temporal_hints, int gpu_id, bool is_reverse, bool use_half_precision, bool save_as_img, std::filesystem::path fpath) : \
perf(perf), cost_buffer(cost_buffer), external_hints(external_hints), temporal_hints(temporal_hints), is_reverse(is_reverse), use_half_precision(use_half_precision), save_as_img(save_as_img) \
{
    std::filesystem::path pathObj(fpath);
    cout << "Loading file: " << fpath.string() << endl;
    imreadmulti(fpath.string(), multi_frame, IMREAD_UNCHANGED);
    parent_dir = dirnameOf(fpath.string());

    if (is_reverse) {
        parent_dir /= ("NvidiaOF_" + perf + "_reverse");
    }
    else {
        parent_dir /= ("NvidiaOF_" + perf);
    }
    
    std::filesystem::create_directory(parent_dir.c_str());
    cout << "Output Directory: " << parent_dir << endl;
};