#include <lyra/lyra.hpp>

#include "OptFlow.hpp"

using namespace std;

int main(int argc, const char** argv)
{
    auto cli = lyra::cli();
    std::string command;
    bool show_help = false;
    cli.add_argument(lyra::help(show_help));
    pyrLK_command pyrLK{ cli };
    farneback_command farneback{ cli };
    brox_command brox{ cli };
    DualTVL1_command dualTVL1{ cli };
    NvidiaOF_command nvidiaOF{ cli };
    auto result = cli.parse({ argc, argv });
    if (show_help)
    {
        std::cout << cli;
        return 0;
    }
    if (!result)
    {
        std::cerr << result.errorMessage() << "\n";
    }
    return result ? 0 : 1;
}