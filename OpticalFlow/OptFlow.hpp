#ifndef OPTFLOW_H
#define OPTFLOW_H
#include <vector>

# if __has_include(<filesystem>)
#   include <filesystem>
# else
#   include <experimental/filesystem>
namespace std {
    namespace filesystem = experimental::filesystem;
}
# endif

#include <lyra/lyra.hpp>
#include <opencv2/core.hpp>

using namespace std;
using namespace cv;
using namespace cv::cuda;

class OptFlow {
public:
    string padZero(int);
    void writeFlow(std::filesystem::path, int, const GpuMat&, bool, bool);
    void matwrite(const string&, const Mat&);
    string to_precision(double, int);
    string dirnameOf(const string&);
    string remove_extension(string);

    void writeCost(std::filesystem::path, int, const GpuMat&);
    Mat imadjust(const Mat, Vec2d);
    Mat loadImage(Mat);

    tuple<GpuMat, GpuMat, GpuMat> loadImagePair(int, vector<Mat>, bool);
};

class PyrLK : public OptFlow {
private:
    int winSize;
    int numLevels;
    int numIters;

    bool is_reverse;
    bool use_half_precision;
    bool save_as_img;
    std::filesystem::path parent_dir;

    vector<Mat> multi_frame;

public:
    void runOptFlow();
    PyrLK(int, int, int, bool, bool, bool, std::filesystem::path);
};

struct pyrLK_command
{
    String fpath;
    int winSize = 13;
    int numLevels = 3;
    int numIters = 30;

    bool is_reverse = false;
    bool use_half_precision = false;
    bool save_as_img = false;

    bool show_help = false;

    pyrLK_command(lyra::cli& cli)
    {
        cli.add_argument(
            lyra::command("PyrLK",
                [this](const lyra::group& g) { this->do_command(g); })
            .help("Calculate vector field using Pyramidal Lucas Kanade optical flow.")
            .add_argument(lyra::help(show_help))
            .add_argument(
                lyra::opt(winSize, "winSize")
                .name("-w")
                .name("--winSize")
                .optional()
                .help("Specify windows size."))
            .add_argument(
                lyra::opt(numLevels, "numLevels")
                .name("-l")
                .name("--numLevels")
                .optional()
                .help("Specify maximum number of pyramidal levels."))
            .add_argument(
                lyra::opt(numIters, "numIters")
                .name("-i")
                .name("--numIters")
                .optional()
                .help("Specify number of iterations."))
            .add_argument(
                lyra::opt(is_reverse)
                .name("--is_reverse")
                .optional()
                .help("Specify if image should be analyzed in the reverse direction."))
            .add_argument(
                lyra::opt(use_half_precision)
                .name("--use_half_precision")
                .optional()
                .help("Specify if results should be save as a 16bit float (half precision)."))
            .add_argument(
                lyra::opt(save_as_img)
                .name("-s")
                .name("--save_as_img")
                .optional()
                .help("Specify if vectors should be saved as a tif image instead of a bin file."))
            .add_argument(
                lyra::arg(fpath, "fpath")
                .required()
                .help("File path of image.")));
    }

    void do_command(const lyra::group& g)
    {
        if (show_help)
            std::cout << g;
        else
        {
            PyrLK optcalc(winSize, numLevels, numIters, is_reverse, use_half_precision, save_as_img, fpath);
            optcalc.runOptFlow();
        }
    }
};

class Farneback : public OptFlow {
private:
    int numLevels;
    double pyrScale;
    bool fastPyramids;
    int winSize;
    int numIters;
    int polyN;
    double polySigma;

    bool is_reverse;
    bool use_half_precision;
    bool save_as_img;
    std::filesystem::path parent_dir;

    vector<Mat> multi_frame;

public:
    void runOptFlow();
    Farneback(int, double, bool, int, int, int, double, bool, bool, bool, std::filesystem::path);
};


struct farneback_command
{
    String fpath;
    int numLevels = 5;
    double pyrScale = 0.5;
    bool fastPyramids = false;
    int winSize = 13;
    int numIters = 10;
    int polyN = 5;
    double polySigma = 1.1;

    bool is_reverse = false;
    bool use_half_precision = false;
    bool save_as_img = false;

    bool show_help = false;

    farneback_command(lyra::cli& cli)
    {
        cli.add_argument(
            lyra::command("Farneback",
                [this](const lyra::group& g) { this->do_command(g); })
            .help("Calculate vector field using Farneback optical flow.")
            .add_argument(lyra::help(show_help))
            .add_argument(
                lyra::opt(winSize, "winSize")
                .name("-w")
                .name("--winSize")
                .optional()
                .help("Specify windows size."))
            .add_argument(
                lyra::opt(numLevels, "numLevels")
                .name("-l")
                .name("--numLevels")
                .optional()
                .help("Specify maximum number of pyramidal levels."))
            .add_argument(
                lyra::opt(numIters, "numIters")
                .name("-i")
                .name("--numIters")
                .optional()
                .help("Specify number of iterations."))
            .add_argument(
                lyra::opt(pyrScale, "pyrScale")
                .name("--pyrScale")
                .optional()
                .help("Specify scale used to generate pyramid."))
            .add_argument(
                lyra::opt(polyN, "polyN")
                .name("--polyN")
                .optional()
                .help("Specify number of polynomial coefficients."))
            .add_argument(
                lyra::opt(polySigma, "polySigma")
                .name("--polySigma")
                .optional()
                .help("Specify value of sigma of the Gaussian filter used to smooth derivatives."))
            .add_argument(
                lyra::opt(fastPyramids)
                .name("--fastPyramids")
                .optional()
                .help("Specify if fast pyramids should be used."))
            .add_argument(
                lyra::opt(is_reverse)
                .name("--is_reverse")
                .optional()
                .help("Specify if image should be analyzed in the reverse direction."))
            .add_argument(
                lyra::opt(use_half_precision)
                .name("--use_half_precision")
                .optional()
                .help("Specify if results should be save as a 16bit float (half precision)."))
            .add_argument(
                lyra::opt(save_as_img)
                .name("-s")
                .name("--save_as_img")
                .optional()
                .help("Specify if vectors should be saved as a tif image instead of a bin file."))
            .add_argument(
                lyra::arg(fpath, "fpath")
                .required()
                .help("File path of image.")));
    }

    void do_command(const lyra::group& g)
    {
        if (show_help)
            std::cout << g;
        else
        {
            Farneback optcalc(numLevels, pyrScale, fastPyramids, winSize, numIters, polyN, polySigma, is_reverse, use_half_precision, save_as_img, fpath);
            optcalc.runOptFlow();
        }
    }
};

class Brox : public OptFlow {
private:
    double alpha;
    double gamma;
    double pyrScale;
    int innerIter;
    int outerIter;
    int solverIter;

    bool is_reverse;
    bool use_half_precision;
    bool save_as_img;
    std::filesystem::path parent_dir;

    vector<Mat> multi_frame;

public:
    void runOptFlow();
    Brox(double, double, double, int, int, int, bool, bool, bool, std::filesystem::path);
};


struct brox_command
{
    String fpath;
    double alpha = 0.197;
    double gamma = 50.0;
    double pyrScale = 0.8;
    int innerIter = 5;
    int outerIter = 150;
    int solverIter = 10;

    bool is_reverse = false;
    bool use_half_precision = false;
    bool save_as_img = false;

    bool show_help = false;

    brox_command(lyra::cli& cli)
    {
        cli.add_argument(
            lyra::command("Brox",
                [this](const lyra::group& g) { this->do_command(g); })
            .help("Calculate vector field using Brox optical flow.")
            .add_argument(lyra::help(show_help))
            .add_argument(
                lyra::opt(alpha, "alpha")
                .name("-a")
                .name("--alpha")
                .optional()
                .help("Specify smoothness parameter."))
            .add_argument(
                lyra::opt(gamma, "gamma")
                .name("-g")
                .name("--gamma")
                .optional()
                .help("Specify weight between grey value and the gradient constancy assumption."))
            .add_argument(
                lyra::opt(pyrScale, "pyrScale")
                .name("--pyrScale")
                .optional()
                .help("Specify scale used to generate pyramid."))
            .add_argument(
                lyra::opt(innerIter, "innerIter")
                .name("--innerIter")
                .optional()
                .help("Specify number of inner iterations."))
            .add_argument(
                lyra::opt(outerIter, "outerIter")
                .name("--outerIter")
                .optional()
                .help("Specify number of outer iterations."))
            .add_argument(
                lyra::opt(solverIter, "solverIter")
                .name("--solverIter")
                .optional()
                .help("Specify number of solver iterations."))
            .add_argument(
                lyra::opt(is_reverse)
                .name("--is_reverse")
                .optional()
                .help("Specify if image should be analyzed in the reverse direction."))
            .add_argument(
                lyra::opt(use_half_precision)
                .name("--use_half_precision")
                .optional()
                .help("Specify if results should be save as a 16bit float (half precision)."))
            .add_argument(
                lyra::opt(save_as_img)
                .name("-s")
                .name("--save_as_img")
                .optional()
                .help("Specify if vectors should be saved as a tif image instead of a bin file."))
            .add_argument(
                lyra::arg(fpath, "fpath")
                .required()
                .help("File path of image.")));
    }

    void do_command(const lyra::group& g)
    {
        if (show_help)
            std::cout << g;
        else
        {
            Brox optcalc(alpha, gamma, pyrScale, innerIter, outerIter, solverIter, is_reverse, use_half_precision, save_as_img, fpath);
            optcalc.runOptFlow();
        }
    }
};

class DualTVL1 : public OptFlow {
private:
    double tau;
    double lambda;
    double theta;
    int numLevels;
    int numWarps;
    double epsilon;
    int numIters;
    double pyrScale;
    double gamma;

    bool is_reverse;
    bool use_half_precision;
    bool save_as_img;
    std::filesystem::path parent_dir;

    vector<Mat> multi_frame;

public:
    void runOptFlow();
    DualTVL1(double, double, double, int, int, double, int, double, double, bool, bool, bool, std::filesystem::path);
};


struct DualTVL1_command
{
    String fpath;
    double tau = 0.25;
    double lambda = 0.15;
    double theta = 0.3;
    int numLevels = 5;
    int numWarps = 5;
    double epsilon = 0.01;
    int numIters = 300;
    double pyrScale = 0.8;
    double gamma = 0.0;

    bool is_reverse = false;
    bool use_half_precision = false;
    bool save_as_img = false;

    bool show_help = false;

    DualTVL1_command(lyra::cli& cli)
    {
        cli.add_argument(
            lyra::command("DualTVL1",
                [this](const lyra::group& g) { this->do_command(g); })
            .help("Calculate vector field using DualTVL1 optical flow.")
            .add_argument(lyra::help(show_help))
            .add_argument(
                lyra::opt(tau, "tau")
                .name("-t")
                .name("--tau")
                .optional()
                .help("Specify smoothness parameter."))
            .add_argument(
                lyra::opt(gamma, "lambda")
                .name("-l")
                .name("--lambda")
                .optional()
                .help("Specify attachment parameter."))
            .add_argument(
                lyra::opt(theta, "theta")
                .name("-t")
                .name("--theta")
                .optional()
                .help("Specify variable to allow for illumination variations."))
            .add_argument(
                lyra::opt(numLevels, "numLevels")
                .name("-n")
                .name("--numLevels")
                .optional()
                .help("Specify maximum number of pyramidal levels."))
            .add_argument(
                lyra::opt(numWarps, "numWarps")
                .name("-w")
                .name("--numWarps")
                .optional()
                .help("Specify number of warps per scale."))
            .add_argument(
                lyra::opt(numIters, "numIters")
                .name("--numIters")
                .optional()
                .help("Specify number of iterations."))
            .add_argument(
                lyra::opt(pyrScale, "pyrScale")
                .name("--pyrScale")
                .optional()
                .help("Specify scale used to generate pyramid."))
            .add_argument(
                lyra::opt(gamma, "epsilon")
                .name("-e")
                .name("--epsilon")
                .optional()
                .help("Specify threshold for stopping criteria."))
            .add_argument(
                lyra::opt(theta, "gamma")
                .name("-g")
                .name("--gamma")
                .optional()
                .help("Specify tightness parameter."))
            .add_argument(
                lyra::opt(is_reverse)
                .name("--is_reverse")
                .optional()
                .help("Specify if image should be analyzed in the reverse direction."))
            .add_argument(
                lyra::opt(use_half_precision)
                .name("--use_half_precision")
                .optional()
                .help("Specify if results should be save as a 16bit float (half precision)."))
            .add_argument(
                lyra::opt(save_as_img)
                .name("-s")
                .name("--save_as_img")
                .optional()
                .help("Specify if vectors should be saved as a tif image instead of a bin file."))
            .add_argument(
                lyra::arg(fpath, "fpath")
                .required()
                .help("File path of image.")));
    }

    void do_command(const lyra::group& g)
    {
        if (show_help)
            std::cout << g;
        else
        {
            DualTVL1 optcalc(tau, lambda, theta, numLevels, numWarps, epsilon, numIters, pyrScale, gamma, is_reverse, use_half_precision, save_as_img, fpath);
            optcalc.runOptFlow();
        }
    }
};

class NvidiaOF : public OptFlow {
private:
    bool cost_buffer;
    bool external_hints;
    bool temporal_hints;

    int gpu_id;
    string perf;

    bool is_reverse;
    bool use_half_precision;
    bool save_as_img;
    std::filesystem::path parent_dir;

    vector<Mat> multi_frame;

public:
    void runOptFlow();
    NvidiaOF(string, bool, bool, bool, int, bool, bool, bool, std::filesystem::path);
};


struct NvidiaOF_command
{
    String fpath;
    bool cost_buffer = false;
    bool external_hints = false;
    bool temporal_hints = false;

    int gpu_id = 0;
    string perf = "Slow";

    bool is_reverse = false;
    bool use_half_precision = false;
    bool save_as_img = false;

    bool show_help = false;

    NvidiaOF_command(lyra::cli& cli)
    {
        cli.add_argument(
            lyra::command("NvidiaOF",
                [this](const lyra::group& g) { this->do_command(g); })
            .help("Calculate vector field using Nvidia HW optical flow (1.0).")
            .add_argument(lyra::help(show_help))
            .add_argument(
                lyra::opt(perf, "perf")
                .name("--perf")
                .choices("Slow", "Medium", "Fast")
                .required()
                .help("Specify if cost buffer should be output."))
            .add_argument(
                lyra::opt(cost_buffer)
                .name("--cost_buffer")
                .optional()
                .help("Specify if cost buffer should be output."))
            .add_argument(
                lyra::opt(temporal_hints)
                .name("--temporal_hints")
                .optional()
                .help("Specify if previous frame should be used as internal hints."))
            .add_argument(
                lyra::opt(gpu_id, "gpu_id")
                .name("--gpu_id")
                .optional()
                .help("Specify which gpu to run optical flow calculation on."))
            .add_argument(
                lyra::opt(is_reverse)
                .name("--is_reverse")
                .optional()
                .help("Specify if image should be analyzed in the reverse direction."))
            .add_argument(
                lyra::opt(use_half_precision)
                .name("--use_half_precision")
                .optional()
                .help("Specify if results should be save as a 16bit float (half precision)."))
            .add_argument(
                lyra::opt(save_as_img)
                .name("-s")
                .name("--save_as_img")
                .optional()
                .help("Specify if vectors should be saved as a tif image instead of a bin file."))
            .add_argument(
                lyra::arg(fpath, "fpath")
                .required()
                .help("File path of image.")));
    }

    void do_command(const lyra::group& g)
    {
        if (show_help)
            std::cout << g;
        else
        {
            NvidiaOF optcalc(perf, cost_buffer, external_hints, temporal_hints, gpu_id, is_reverse, use_half_precision, save_as_img, fpath);
            optcalc.runOptFlow();
        }
    }
};

#endif